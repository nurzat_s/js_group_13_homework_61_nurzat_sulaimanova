import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent {
  modalOpen = false;

  constructor(private router: Router) { }

  closeCheckoutModal() {
    this.modalOpen = false;
  }

  openCheckoutModal() {
    this.modalOpen = true;
  }

  send() {
    void this.router.navigate(['/checkout']);
  }
}
